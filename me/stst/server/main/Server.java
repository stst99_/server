package me.stst.server.main;

import sun.invoke.empty.Empty;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by xy on 22.03.2016.
 */
public class Server extends Thread{
    private ServerSocket serverSocket;
    private List<ClientThread> clients=new CopyOnWriteArrayList<>();
    private final String password;
    private Map<Channel, ArrayList<InputEventListener>> listeners=new ConcurrentHashMap<>();
    private List<Channel> channels= new CopyOnWriteArrayList<>();
    private Map<String, ArrayList<Object>> storage=new ConcurrentHashMap<>();
    private Logger logger=new Logger();
    private static boolean whitelist=false;
    private static List<InetAddress> whitelisted=new CopyOnWriteArrayList<>();
    private static long nextClientID=0;
    private boolean stop=false;

    public Server(int port, final String password){
        try {
            serverSocket=new ServerSocket(port);
            serverSocket.setSoTimeout(100000);
        }catch (IOException ex){
            ex.printStackTrace();
        }
        this.password=password;
        Channel main=new Channel(Channel.ChannelType.C_S, "MAIN");
        Channel broadcast=new Channel(Channel.ChannelType.C_S_AC, "BROADCAST");
        channels.add(broadcast);
        channels.add(main);
        new Console().start();
        this.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
            }
        });
        registerListener(new InputEventListener() {
            @Override
            public void onMessageReceived(Message received, ClientThread from) {
                if (from.isLoggedIn()){
                    for (ClientThread thread:clients){
                        if (thread.isLoggedIn()){
                            thread.send(received);
                        }
                    }
                }
            }
        }, broadcast);
        registerListener(new MainListener(),main );
    }


    synchronized public void registerListener(InputEventListener listener, Channel channel){
        if (this.listeners.get(channel)==null){
            listeners.put(channel, new ArrayList<InputEventListener>());
            this.listeners.get(channel).add(listener);
        }else {
            this.listeners.get(channel).add(listener);
        }
    }
    synchronized public void addChannel(Channel channel){
        this.channels.add(channel);
    }

    synchronized public void addChannelRemote(Channel channel){
        if (channel==null||channel.getType()==null||channel.getName()==null){
            throw new NullPointerException("Something is null");
        }
        for (Channel tempc:channels){
            if (tempc.getName().equals(channel.getName())){
                return;
            }
        }
        this.channels.add(channel);
        switch (channel.getType()){
            case C_S:{
                this.registerListener(new InputEventListener() {
                    @Override
                    public void onMessageReceived(Message received, ClientThread from) {
                        if (from.isLoggedIn()){
                            from.send(new Message(received.getChannel(), "SEND OK"));
                            logger.log(Logger.Level.INFO, "Got message from "+from.getClientName()+
                                    ": "+received.toString(), System.currentTimeMillis());
                        }
                    }
                }, channel);
                break;
            }

            case C_S_AC:{
                this.registerListener(new InputEventListener() {
                    @Override
                    public void onMessageReceived(Message received, ClientThread from) {
                        if (from.isLoggedIn()){
                            sendToAll(received);
                            from.send(new Message(received.getChannel(), "SEND OK"));
                        }
                    }
                }, channel);
                break;
            }
            case C_S_C:{
                this.registerListener(new InputEventListener() {
                    @Override
                    public void onMessageReceived(Message received, ClientThread from) {
                        if (from.isLoggedIn()){
                            String[] args=received.getMessage().split(" ");
                            if (args.length>=1){
                                ClientThread clientThread=getByName(args[1]);
                                if (clientThread!=null&&clientThread.isLoggedIn()){
                                    clientThread.send(received);
                                    from.send(new Message(received.getChannel(), "SEND OK"));
                                }else {
                                    from.send(new Message(received.getChannel(), "SEND FAIL UNKNOWN CLIENT"));
                                }
                            }else {
                                from.send(new Message(received.getChannel(), "SEND FAIL TO FEW ARGS"));
                            }
                        }
                    }
                }, channel);
                break;
            }
        }
    }

    synchronized public void sendToAll(Message s){
        Iterator<ClientThread> clients=this.clients.iterator();
        while (clients.hasNext()){
            ClientThread thread=clients.next();
            if (thread.isLoggedIn()){
                thread.send(s);
            }
        }
    }
    synchronized public void parseInput(Message message, ClientThread thread){
            for (Channel c: channels){
                //channelsit.remove();
                if (c.getName().equals(message.getChannel())){
                    for (InputEventListener listener:listeners.get(c)){
                        listener.onMessageReceived(message, thread);
                    }
                }
            }
    }

    synchronized public void removeClient(ClientThread thread){
        this.clients.remove(thread);
    }

    @Deprecated
    private void sendToSocket(Socket socket, Message msg){
        try {
            ObjectOutputStream output= new ObjectOutputStream(socket.getOutputStream());
            output.writeObject(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (!stop){
            try {
                Socket newClient=serverSocket.accept();
                if (whitelist){
                    boolean added=false;
                    for (InetAddress address:whitelisted){
                        if (address.toString().equals(newClient.getInetAddress().toString())){
                            newClient.setKeepAlive(true);
                            logger.log(Logger.Level.INFO,"Got connection request form "+newClient.getInetAddress().toString(),
                                    System.currentTimeMillis());
                            new ClientThread(newClient, "not_logged_in", this);
                            added=true;
                            break;
                        }
                    }
                    if (!added){
                        logger.log(Logger.Level.INFO, newClient.getInetAddress()+" tried to connect, but is not whitelisted",
                                System.currentTimeMillis());
                        newClient.close();
                    }
                }else {
                    newClient.setKeepAlive(true);
                    logger.log(Logger.Level.INFO,"Got connection request form "+newClient.getInetAddress().toString(),
                            System.currentTimeMillis());
                    new ClientThread(newClient, "not_logged_in", this);
                }
            } catch (IOException e) {
                if (!e.getMessage().equalsIgnoreCase("Accept timed out")&&!stop){
                    logger.log(Logger.Level.ERROR, "An error occurred: "+ e.getMessage(), System.currentTimeMillis());
                }
            }
        }
    }

    public String parseArgs(String[] args, int begin){
        StringBuilder ret=new StringBuilder();
        for (int i=begin; i<args.length; i++){
            ret.append(args[i]+" ");
        }
        return ret.toString().trim();
    }

    public ClientThread getByName(String name){
        ClientThread ret=null;
        for (ClientThread clientThread:clients){
            if (name.equals(clientThread.getClientName())){
                ret=clientThread;
            }
        }
        return ret;
    }

    public ArrayList<Channel> clone(ArrayList<Channel> arrayList){
        ArrayList<Channel> ret=new ArrayList();
        for (Channel o: arrayList){
            ret.add(o);
        }
        return ret;
    }

    public List<ClientThread> getClients() {
        return clients;
    }

    public void setClients(List<ClientThread> clients) {
        this.clients = clients;
    }

    public String getPassword() {
        return password;
    }

    public Map<Channel, ArrayList<InputEventListener>> getListeners() {
        return listeners;
    }

    public void setListeners(HashMap<Channel, ArrayList<InputEventListener>> listeners) {
        this.listeners = listeners;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }

    public Map<String, ArrayList<Object>> getStorage() {
        return storage;
    }

    public void setStorage(HashMap<String, ArrayList<Object>> storage) {
        this.storage = storage;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public static long getNextClientID() {
        return nextClientID;
    }

    public static void setNextClientID(long nextClientID) {
        Server.nextClientID = nextClientID;
    }

    public ClientThread getById(long id){
        for (ClientThread thread:clients){
            if (thread.getId()==id){
                return thread;
            }
        }
        return null;
    }

    public class Console extends Thread{

        public Console(){
            this.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    e.printStackTrace();
                    run();
                }
            });
        }

        @Override
        public void run() {
            while (true){
                String s=new Scanner(System.in).nextLine();
                String[] command =s.split(" ");
                if (command.length>=1){
                    switch (command[0].toLowerCase()){
                        case "list":{
                            for (ClientThread thread:clients){
                                logger.log(Logger.Level.INFO, thread.getClientName()+" ID:"
                                        +Long.toString(thread.getId()), System.currentTimeMillis());
                            }
                            break;
                        }
                        case "info":{
                            if (command.length>=2){
                                try {
                                    ClientThread client=getById(Long.parseLong(command[1]));
                                    if (client!=null){
                                        logger.log(Logger.Level.INFO, client.getClientName()+": ID: "+
                                                        client.getId()+" IP: "+client.getSocket().getInetAddress().toString(),
                                                System.currentTimeMillis());
                                    }else {
                                        logger.log(Logger.Level.INFO, "Unknown client", System.currentTimeMillis());
                                    }
                                }catch (NumberFormatException ex){
                                    logger.log(Logger.Level.INFO, "Invalid input!", System.currentTimeMillis());
                                }
                            }
                            break;
                        }
                        case "kick":{
                            if (command.length>=2){
                                try {
                                    ClientThread client=getById(Long.parseLong(command[1]));
                                    if (client!=null){
                                        client.setStop(true);
                                        removeClient(client);
                                        try {
                                            client.getSocket().close();
                                        } catch (IOException e) {

                                        }
                                    }else {
                                        logger.log(Logger.Level.INFO, "Unknown client", System.currentTimeMillis());
                                    }
                                }catch (NumberFormatException ex){
                                    logger.log(Logger.Level.INFO, "Invalid input!", System.currentTimeMillis());
                                }
                            }
                            break;
                        }
                        case "stop":{
                            try {
                                stop=true;
                                serverSocket.close();
                                System.exit(0);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case "whitelist":{
                            if (command.length>=2){
                                switch (command[1].toLowerCase()){
                                    case "on":{
                                        whitelist=true;
                                        break;
                                    }
                                    case "off":{
                                        whitelist=false;
                                        break;
                                    }
                                    case "add":{
                                        if (command.length>=3){
                                            try {
                                                whitelisted.add(InetAddress.getByName(command[2]));
                                                logger.log(Logger.Level.INFO, "Added "+command[2]+" to the whitelist",
                                                        System.currentTimeMillis());
                                            } catch (UnknownHostException e) {
                                                logger.log(Logger.Level.INFO, "Unknown host", System.currentTimeMillis());
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case "channellist":{
                            logger.log(Logger.Level.INFO, "Channel list:", System.currentTimeMillis());
                            for (Channel channel:channels){
                                logger.log(Logger.Level.INFO, "Name: "+channel.getName()+" Type: "+
                                        channel.getType().toString(), System.currentTimeMillis());
                            }
                            break;
                        }
                        case "debug":{
                            logger.setDebug(!logger.isDebug());
                            logger.log(Logger.Level.INFO, "Debug toggled", System.currentTimeMillis());
                            break;
                        }
                        default:{
                            logger.log(Logger.Level.INFO, "Unknown command", System.currentTimeMillis());
                            break;
                        }
                    }
                }
            }
        }
    }


    public static void main(String[] args) {
        if (args.length>=2){
            int port=0;
            try {
                port=Integer.parseInt(args[0]);
            }catch (NumberFormatException ex){
                System.err.println("Your given port is not a number!");
                System.exit(0);
            }

            new Server(port, args[1]).start();
            for (int i=1; i<args.length; i++){
                String arg=args[i];
                switch (arg.toLowerCase()){
                    case "whitelist":{
                        whitelist=true;
                        File wl=new File("whitelist.txt");
                        if (wl.exists()){
                            try {
                                BufferedReader reader=new BufferedReader(new FileReader(wl));
                                while (true){
                                    String r=reader.readLine();
                                    if (r==null){
                                        break;
                                    }else {
                                        whitelisted.add(InetAddress.getByName(r));
                                    }
                                }
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }
                }
            }
        }else {
            System.err.println("To few arguments!");
        }
    }
}
