package me.stst.server.main;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xy on 25.03.2016.
 */
public class Channel {
    private ChannelType type;
    private String name;

    public Channel(ChannelType type, String name) {
        this.type = type;
        this.name = name;
    }

    public ChannelType getType() {
        return type;
    }

    public void setType(ChannelType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> serialize() {
        Map<String, Object> ret=new HashMap<>();
        ret.put("type", type.toString());
        ret.put("name", name);
        return ret;
    }

    public static Channel deSerialize(Map<String, Object> s){
        return new Channel(ChannelType.valueOf((String) s.get("type")), (String)s.get("name"));
    }

    public enum ChannelType{C_S, C_S_C, C_S_AC}
}
