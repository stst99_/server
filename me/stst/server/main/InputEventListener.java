package me.stst.server.main;


/**
 * Created by xy on 23.03.2016.
 */
public interface InputEventListener {
    void onMessageReceived(Message received, ClientThread from);
}
