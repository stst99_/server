package me.stst.server.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by xy on 23.03.2016.
 */
public class Message{
    private String channel;
    private String message;
    private ArrayList<Object> attachments;

    public Message(String channel, String message, ArrayList<Object> attachments) {
        this.channel = channel;
        this.message = message;
        this.attachments=attachments;
    }



    public Message(String channel, String message) {
        this(channel, message, null);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.getChannel()+" "+this.getMessage();
    }

    public ArrayList<Object> getAttachments() {
        return attachments;
    }

    public void setAttachments(ArrayList<Object> attachments) {
        this.attachments = attachments;
    }

    public Map<String, Object> serialize() {
        Map<String, Object> ret=new HashMap<>();
        ret.put("channel", this.channel);
        ret.put("message", this.message);
        ret.put("attachments", this.attachments);
        return ret;
    }

    public static Message deSerialize(Map<String, Object> objectMap){
        try {
            return new Message((String) objectMap.get("channel"),
                    (String)objectMap.get("message"), (ArrayList<Object>) objectMap.get("attachments"));
        }catch (ClassCastException ex){
            ex.printStackTrace();
            System.err.println("Got wrong message from server, please check your server version!");
            return null;
        }
    }
}
