package me.stst.server.main;

import java.text.SimpleDateFormat;

/**
 * Created by xy on 25.03.2016.
 */
public class Logger {
    boolean debug=false;
    private SimpleDateFormat dateFormat;


    public Logger(String dateformat){
        this.dateFormat=new SimpleDateFormat(dateformat);
    }

    public Logger(){
        this.dateFormat=new SimpleDateFormat();
    }
    public void log(Level level, String msg, long time){
        if (level.equals(Level.DEBUG)&&debug){
            System.out.println("["+dateFormat.format(time)+" : "+level.toString()+"] "+msg);
        }
        if (!level.equals(Level.DEBUG)){
            System.out.println("["+dateFormat.format(time)+" : "+level.toString()+"] "+msg);
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public enum Level{ERROR, INFO, DEBUG, WARN}
}
