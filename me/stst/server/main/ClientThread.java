package me.stst.server.main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;

/**
 * Created by xy on 01.04.2016.
 */
public class ClientThread extends Thread{
    private Socket socket;
    private String clientName;
    private boolean stop=false;
    private boolean loggedIn=false;
    private boolean nameSet=false;
    private final long ID;
    private Server server;
    private Logger logger;

    public ClientThread(Socket socket, String name, Server server){
        this.server=server;
        this.socket=socket;
        this.clientName =name;
        server.getClients().add(this);
        ID =Server.getNextClientID();
        Server.setNextClientID(ID+1);
        logger=server.getLogger();
        this.start();
        new PingThread().start();
    }

    public void sleepClient(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            logger.log(Logger.Level.ERROR,"Sleep was Interrupted, stopping", System.currentTimeMillis());
            this.stop=true;
            server.removeClient(this);
        }
    }

    public Server getServer() {
        return server;
    }

    @Override
    public long getId() {
        return ID;
    }

    public void ping(){
        send(new Message("MAIN", "PING"));
    }

    public void send(Message s){
        try {
            if (socket.isConnected()){
                ObjectOutputStream output=new ObjectOutputStream(socket.getOutputStream());
                output.writeObject(s.serialize());
            }else {
                socket.close();
                server.removeClient(this);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.log(Logger.Level.WARN, "Unable to send a message to "+this.getClientName(), System.currentTimeMillis());
            server.removeClient(this);
            this.stop=true;
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public String getClientName() {
        return clientName;
    }

    public boolean setClientName(String name) {
        if (!this.nameSet){
            this.clientName = name;
            this.nameSet=true;
            return true;
        }else {
            return false;
        }
    }

    public boolean isNameSet() {
        return nameSet;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    @Override
    public void run() {
        while (!stop){
            //send(new Message("MAIN", "PING"));
            try{
                ObjectInputStream input=new ObjectInputStream(socket.getInputStream());
                Object in=input.readObject();
                logger.log(Logger.Level.DEBUG, in.toString(), System.currentTimeMillis());
                if (in instanceof Map){
                    Message message=Message.deSerialize((Map) in);
                    server.parseInput(message, this);
                }
            } catch (IOException e) {
                if (!stop){
                    this.stop=true;
                    server.removeClient(this);
                    logger.log(Logger.Level.WARN,"Client "+this.getClientName()+
                            " disconnected with message: "+e.getMessage(), System.currentTimeMillis());
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                this.stop=true;
            }
        }
    }

    private class PingThread extends Thread{

        @Override
        public void run() {
            while (!stop){
                ping();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
