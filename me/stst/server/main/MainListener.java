package me.stst.server.main;

import java.io.IOException;
import java.util.Map;

/**
 * Created by xy on 01.04.2016.
 */
public class MainListener implements InputEventListener {

    public void onMessageReceived(Message received, ClientThread from) {
        Logger logger=from.getServer().getLogger();
        String[] args=received.getMessage().split(" ");
        if (args.length>=1){
            switch (args[0]){
                case "LOGIN":{
                    if (args.length>=3){
                        if (!from.isNameSet()||args[1].equals(from.getClientName())){
                            if (args[2].equals(from.getServer().getPassword())){
                                /*for (ClientThread thread:from.getServer().getClients()){
                                    if (thread.getClientName().equals(from.getClientName())){
                                        from.send(new Message("MAIN","LOGIN FAIL"));
                                        logger.log(Logger.Level.INFO, from.getClientName()+" tried to login, but the " +
                                                "client name is already used", System.currentTimeMillis());
                                        return;
                                    }
                                }*/
                                from.setLoggedIn(true);
                                from.setClientName(args[1]);
                                from.send(new Message("MAIN","LOGIN SUCCEEDED"));
                                logger.
                                        log(Logger.Level.INFO, from.getClientName()+" logged in", System.currentTimeMillis());
                            }else {
                                from.send(new Message("MAIN","LOGIN FAIL"));
                                logger.log(Logger.Level.INFO, from.getClientName()+" has send the wrong password",
                                        System.currentTimeMillis());
                                from.sleepClient(5000);
                            }
                        }else {
                            from.send(new Message("MAIN", "LOGIN FAIL"));
                        }
                    }else {
                        from.send(new Message("MAIN", "LOGIN NOT ENOUGH ARGUMENTS"));
                    }
                    break;
                }
                case "LOGOUT":{
                    from.setStop(true);
                    try {
                        from.send(new Message("MAIN", "LOGOUT OK"));
                        from.getSocket().close();
                        logger.log(Logger.Level.INFO, from.getClientName()+" has logged out", System.currentTimeMillis());
                        from.getServer().removeClient(from);
                    } catch (IOException e) {}
                    break;
                }
                case "NEW_CHANNEL":{
                    if (from.isLoggedIn()){
                        if (received.getAttachments()!=null){
                            for (Object o:received.getAttachments()){
                                if (o instanceof Map){
                                    final Channel channel=Channel.deSerialize((Map) o);
                                    try {
                                        from.getServer().addChannelRemote(channel);
                                        from.send(new Message("MAIN", "NEW_CHANNEL OK "+channel.getName()));
                                    }catch (NullPointerException ex){
                                        from.send(new Message("MAIN", "NEW_CHANNEL FAIL "+ex.getMessage()));
                                    }
                                }
                            }
                        }else {
                            from.send(new Message("MAIN", "NEW_CHANNEL FAIL ATTACHMENT IS NULL"));
                        }
                    }
                    break;
                }
                case "PUT":{
                    if (from.isLoggedIn()){
                        if (args.length>=2){
                            from.getServer().getStorage().put(args[1], received.getAttachments());
                            from.send(new Message("MAIN", "PUT OK"));
                        }else {
                            from.send(new Message("MAIN", "PUT FAIL TO FEW ARGS"));
                        }
                    }
                    break;
                }
                case "GET":{
                    if (from.isLoggedIn()){
                        if (args.length>=2){
                            from.send(new Message("MAIN", "GET "+args[1], from.getServer().getStorage().get(args[1])));
                        }else {
                            from.send(new Message("MAIN", "GET FAIL TO FEW ARGS"));
                        }
                    }
                    break;
                }
                case "CLIENTS":{
                    if (from.isLoggedIn()){
                        StringBuilder send=new StringBuilder();
                        for (ClientThread thread:from.getServer().getClients()){
                            send.append(thread.getClientName()+";");
                        }
                        from.send(new Message("MAIN", "CLIENTS "+send.toString()));
                    }
                    break;
                }
            }
        }
    }
}
